package zoo;

public interface Animal {
    void printName();

    void printSound();
}