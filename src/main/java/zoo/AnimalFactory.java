package zoo;

import lombok.AllArgsConstructor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;
import java.util.Properties;

@AllArgsConstructor
public class AnimalFactory {
    private final Properties zooProperties;

    public Animal generateAnimal(String animalPronoun) throws CanNotInitializeAnimalException {
        Optional<String> className = Optional.ofNullable(zooProperties.getProperty(animalPronoun));

        if (className.isEmpty()) {
            throw new CanNotInitializeAnimalException("The pronoun " + animalPronoun + " is not defined in the properties file", new Throwable());
        }

        try {
            Class<?> animalClass = Class.forName(className.get());
            Constructor<?> constructor = animalClass.getConstructor();

            return (Animal) constructor.newInstance();
        } catch (InvocationTargetException exception) {
            throw new CanNotInitializeAnimalException("A method invokes by a constructor in class" + className.get() + " has thrown an exception", exception);
        } catch (IllegalAccessException exception) {
            throw new CanNotInitializeAnimalException("Can't instantiate object of kind" + className.get() + "because constructor is not public", exception);
        } catch (InstantiationException | NoSuchMethodException exception) {
            throw new CanNotInitializeAnimalException("A constructor is missing for class " + className.get() + " ,or pronoun's (" + animalPronoun + ") is of type animal", exception);
        } catch (ClassNotFoundException exception) {
            throw new CanNotInitializeAnimalException("The pronoun's (" + animalPronoun + ") animal type (" + className.get() + ") is unknown", exception);
        }
    }
}
