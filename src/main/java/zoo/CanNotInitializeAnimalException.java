package zoo;

public class CanNotInitializeAnimalException extends Exception {
    public CanNotInitializeAnimalException(String massage, Throwable throwable){
        super(massage, throwable);
    }

    public CanNotInitializeAnimalException(String massage){
        super(massage);
    }

    public CanNotInitializeAnimalException(){
        super();
    }
}
