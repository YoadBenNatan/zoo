package zoo;

import org.apache.velocity.exception.ResourceNotFoundException;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ZooProperties zooProperties = new ZooProperties();
        AnimalFactory animalFactory = new AnimalFactory(zooProperties.getPronounsAndTypesProp());

        try {
            callAnimalPrintNameAndPrintSound(args[0], animalFactory, zooProperties);
        } catch (IOException | CanNotInitializeAnimalException | ResourceNotFoundException exception) {
            exception.printStackTrace();
        }
    }

    private static void callAnimalPrintNameAndPrintSound(String fileName, AnimalFactory animalFactory, ZooProperties zooProperties) throws CanNotInitializeAnimalException, IOException, ResourceNotFoundException {
        zooProperties.loadPronounsAndTypesProperties();
        List<String> animalsStrings = ZooFileReader.toStringArray(fileName);

        for (String animalString : animalsStrings) {
            Animal animal = animalFactory.generateAnimal(animalString);
            animal.printName();
            animal.printSound();
        }
    }
}