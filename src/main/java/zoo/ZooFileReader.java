package zoo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ZooFileReader {
    public static List<String> toStringArray(String fileName) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(fileName));
        List<String> animalStrings = new LinkedList<>();

        while (scanner.hasNext()) {
            animalStrings.add(scanner.next());
        }

        scanner.close();

        return animalStrings;
    }
}