package zoo;

import lombok.Getter;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.*;
import java.util.*;

@Getter
public class ZooProperties {
    private final Properties pronounsAndTypesProp = new Properties();

     private static final String PROPERTIES_FILE_NAME = "zoo.properties";

    public void loadPronounsAndTypesProperties() throws IOException, ResourceNotFoundException {
        try {
            pronounsAndTypesProp.load(new ClasspathResourceLoader().getResourceStream(PROPERTIES_FILE_NAME));
        } catch (IOException exception) {
            throw new IOException("Filename does not corresponding with the fixed value", exception);
        }
    }
}